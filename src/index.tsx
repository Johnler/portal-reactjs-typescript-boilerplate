import React from 'react';
import ReactDOM from 'react-dom';
import './sass/index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';

import { MachineContext } from './context';

const data = 'WORLD';

ReactDOM.render(
  <React.StrictMode>
    <MachineContext.Provider value={data}>
    <App />
    </MachineContext.Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
