import { createContext } from 'react';

const MachineContext = createContext<any>(undefined);

export { MachineContext }