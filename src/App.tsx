import React, { useState } from 'react';
import './sass/_App.scss';
import { BrowserRouter as Router } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { MachineContext } from './context';
import portalMachine from './machine/Users';
import { useMachine } from '@xstate/react';


import SideBar from './base/SideBar';
import Body from './base/Body';
import NavBar from './base/NavBar'

import CssBaseline from '@material-ui/core/CssBaseline';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));



function App() {
  const classes = useStyles();
  const [state, send] = useMachine(portalMachine);
  
  const [states, setStates] = useState({
    sideBar: true
  });


  const handleSideBar = (): any => {
    states.sideBar ? setStates({sideBar: false}) : setStates({sideBar: true}) 
  }



  return (
    <MachineContext.Provider value={[state, send]}>
       <div className={classes.root}>
      <CssBaseline />
      <NavBar
        handleSideBar={handleSideBar}
        open={states.sideBar}
      />
      <Router>
        <SideBar 
          handleSideBar={handleSideBar}
          open={states.sideBar}
        />
        <Body 
          open={states.sideBar}
        />
      </Router>
      </div>
    </MachineContext.Provider>
  );
}

export default App;
