import { Dashboard, Users } from '../pages'

export const routes = [
    {
        path: "/",
        exact: true,
        main: () => <Dashboard />
    },
    {
        path: "/Users",
        exact: true,
        main: () =>  <Users />
    }

]


export const SidebarMenu = [
    {
        to: '/',
        label: 'Dashboard'
    },
    {
        to: '/Users',
        label: 'Users'
    }
]