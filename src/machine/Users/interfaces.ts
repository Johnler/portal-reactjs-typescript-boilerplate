export interface PortalContext {

}


export interface PortalSchema {
    states: {
        dashboard: {};
        users: {
            states: {
                idle: {};
                addUser: {
                    states: {
                        inputAddUser: {};
                        submittingAddUser: {}
                    };
                };
                editUser: {},
                deleteUser: {}
            };
        };

    }
}

