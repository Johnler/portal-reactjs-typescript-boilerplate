export type PortalEvent = 
| { type: 'USERS' }
| { type: 'ADD_USER' }
| { type: 'EDIT_USER' }
| { type: 'DELETE_USER' }
| { type: 'CANCEL_ADDUSER' }
| { type: 'SUBMIT_CREATE_ADDUSER' }
