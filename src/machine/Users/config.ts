

import { MachineConfig } from 'xstate';
import { PortalContext, PortalSchema } from './interfaces'
import { PortalEvent } from './types'


const config: MachineConfig<PortalContext, PortalSchema, PortalEvent> = {
    initial: 'dashboard',
    context: {},
    states: {
      dashboard: {
        on: {
          USERS: {
            target: 'users'
          }
        }
      },
      users: {
        id: 'users',
        initial: 'idle',
        states: {
          idle: {
            invoke: {
              src: 'retrieveUsersData'
            },
            on: {
              ADD_USER: {
                target: 'addUser'
              },
              EDIT_USER: {
                target: 'editUser'
              },
              DELETE_USER: {
                target: 'deleteUser'
              }
            }
          },
          addUser: {
            initial: 'inputAddUser',
            states: {
              inputAddUser: {
                on: {
                  CANCEL_ADDUSER: '#users.idle',
                  SUBMIT_CREATE_ADDUSER: {
                    actions: 'createAddUser',
                    target: 'submittingAddUser'
                  }
                }
              },
              submittingAddUser: {
                invoke: {
                  src: 'submitAddUser',
                  onDone: {
                    target: '#users.idle'
                  },
                  onError: {
                      target: '#users.idle'
                  }
                }
              }
            }
          },
          editUser: {},
          deleteUser: {}
        }
      }
    }
  }


  export default config
  
  
