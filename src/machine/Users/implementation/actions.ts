import {ActionFunctionMap} from 'xstate'
import { PortalContext } from '../interfaces'
import { PortalEvent } from '../types'

const actions: ActionFunctionMap<PortalContext, PortalEvent> = {
    
}

export default actions