import {MachineOptions} from 'xstate'



import { PortalContext } from '../interfaces'
import { PortalEvent } from '../types'

const implementations: MachineOptions<PortalContext, PortalEvent> = {
    actions: {

    },
    activities: {},
    services: {
        retrieveUsersData: (): any => {
            console.log('HOI im running')
        }
    },
    guards: {},
    delays: {}
}

export default implementations