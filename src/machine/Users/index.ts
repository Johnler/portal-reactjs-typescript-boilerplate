import {Machine} from 'xstate'
import config from './config'
import implementations from './implementation'

const portalMachine = Machine(config, implementations) 

export default portalMachine